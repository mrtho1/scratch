'use strict';

let _ = require('lodash');

function Tree(allowDuplicates) {
  this._allowDuplicates = !!allowDuplicates;
}

Tree.prototype.INORDER = Symbol('_inorder');
Tree.prototype.PREORDER = Symbol('_preorder');
Tree.prototype.POSTORDER = Symbol('_postorder');
Tree.prototype.REVERSEORDER = Symbol('_reverseorder');
Tree.prototype.LEVELORDER = Symbol('_levelorder');

Tree.prototype.traverse = function* (order) {
  if (order === Tree.prototype.LEVELORDER) {
    yield* _levelorder(this.root);
  } else if (order === Tree.prototype.PREORDER) {
    yield* _preorder(this.root);
  } else if (order === Tree.prototype.POSTORDER) {
    yield* _postorder(this.root);
  } else if (order === Tree.prototype.REVERSEORDER) {
    yield* _reverseorder(this.root);
  } else {
    yield* _inorder(this.root);
  }
};

Tree.prototype.add = function add(data) {

  if (!this.root) {
    this.root = {
      data: data
    };
  } else {
    _add(this.root, data, this._allowDuplicates);
  }
};

Tree.prototype.values = function values() {
  let v = [];
  for (let x of this) {
    v.push(x);
  }
  return v;
};

Tree.prototype.contains = function contains(data) {
  for (let v of this) {
    if (v === data) {
      return true;
    }
  }

  return false;
};

Tree.prototype.height = () => _height(this.root, false);
Tree.prototype.minHeight = () => _height(this.root, true);

Tree.prototype[Symbol.iterator] = function*() {
  yield* _inorder(this.root);
};

function _height(current, min) {
  if (!current) {
    return 0;
  }

  let lheight = current.left ? _height(current.left, min) : 0;
  let rheight = current.right ? _height(current.right, min) : 0;
  return (min ? Math.min(lheight, rheight) : Math.max(lheight, rheight)) + 1;
}

function _add(current, data, allowduplicates) {

  if (data < current.data || (data === current.data && allowduplicates)) {

    if (!current.left) {
      current.left = {
        data: data
      };
    } else {
      _add(current.left, data);
    }
  } else if (data > current.data) {

    if (!current.right) {
      current.right = {
        data: data
      };
    } else {
      _add(current.right, data);
    }
  }
}

function* _levelorder(start) {
  let state = [];

  state.push(start);

  while (state.length !== 0) {
    let node = state.shift();

    yield node.data;
    if (node.left) {
      state.push(node.left);
    }

    if (node.right) {
      state.push(node.right);
    }
  }
}

function* _reverseorder(node) {
  if (!node) {
    return;
  }

  if (node.right) {
    yield* _reverseorder(node.right);
  }

  yield node.data;

  if (node.left) {
    yield* _reverseorder(node.left);
  }
};

function* _preorder(node) {
  if (!node) {
    return;
  }

  yield node.data;

  if (node.left) {
    yield* _preorder(node.left);
  }

  if (node.right) {
    yield* _preorder(node.right);
  }
};

function* _inorder(node) {
  if (!node) {
    return;
  }

  if (node.left) {
    yield* _inorder(node.left);
  }

  yield node.data;

  if (node.right) {
    yield* _inorder(node.right);
  }
}

function* _postorder(node) {
  if (!node) {
    return;
  }

  if (node.left) {
    yield* _postorder(node.left);
  }

  if (node.right) {
    yield* _postorder(node.right);
  }

  yield node.data;
}

module.exports = Tree;

let t = new Tree();
_.times(20, function() {
  t.add(Math.round(Math.random() * 100));
});

console.log('values   :', t.values());
let contained = [];
for (let i = 0; i <= 100; ++i) {
  if (t.contains(i)) {
    contained.push(i);
  }
}
console.log('contained:', contained.sort((a, b) => a - b));
console.log('height    :', t.height());
console.log('min height:', t.minHeight());

function doTraverse(tree, order, tag) {
  let values = [];
  for (let v of tree.traverse(order)) {
    values.push(v);
  }

  console.log(tag, values);
}

doTraverse(t, t.INORDER,      "in-order      :");
doTraverse(t, t.REVERSEORDER, "reverse-order :");
doTraverse(t, t.PREORDER,     "pre-order     :");
doTraverse(t, t.POSTORDER,    "post-order    :");
doTraverse(t, t.LEVELORDER,   "level-order   :");
