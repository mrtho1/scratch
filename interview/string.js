var assert = require('assert');

function findFirstNonRepeatedChar(theString) {
  var hash = {}, current, result;

  for (current of theString) {
    if (!hash[current]) {
      hash[current] = 1;
    } else {
      ++hash[current]
    }
  }

  for (current of theString) {
    if (hash[current] === 1) {
      result = current;
      break;
    }
  }

  return result;
}

function reverseString(theString) {
  return theString.split('').reverse().join('');
}

function reverseWords(theString) {
  return reverseString(theString).split(' ').map(function(word) {
    return reverseString(word);
  }).join(' ');
}

function toInteger(theString) {
  var sign = 1, num = 0, x;
  var ZERO_CHAR_CODE = '0'.charCodeAt(0);

  for (x = 0; x < theString.length; ++x) {
    if (x === 0 && theString[x] === '-') {
      sign = -1;
    } else {
      var digit = theString[x].charCodeAt(0) - ZERO_CHAR_CODE;
      num *= 10;
      num += digit;
    }
  }

  return sign * num;
}

function itos(theNumber) {
  var result = "";
  var sign = theNumber < 0 ? '-': '';

  theNumber = Math.abs(theNumber);
  do {
    var digit = theNumber % 10;
    theNumber = Math.floor(theNumber / 10);
    result = digit + result;
  } while (theNumber !== 0);

  return sign + result;
}

function permute(theString) {

  function _doPermute(chars, result, used, results) {
    if (chars.length === result.length) {
      results.push(result);
    } else {
      for (var i = 0; i < chars.length; ++i) {
        if (used[i]) {
          continue;
        }

        used[i] = true;
        _doPermute(chars, result + chars[i], used, results);
        used[i] = false;
      }
    }
  }

  var len = theString.length;
  var used = new Array(len);
  used.fill(false);
  var chars = theString.split('');

  var results = [];
  _doPermute(chars, '', used, results);
  return results;
}

function combine(theString) {

  function _doCombine(chars, result, start, results) {

    for (var i = start; i < chars.length; ++i) {
      var next = result + chars[i];
      results.push(next);
      _doCombine(chars, next, i + 1, results);
    }
  }

  var results = [];
  _doCombine(theString.split(''), '', 0, results);
  return results;
}

function debounce(fn, delay) {
  var timeout;

  return function() {
    var args = [].slice.call(arguments);
    if (timeout) {
      clearTimeout(timeout);
      timeout = undefined;
    }

    var cap = this;
    timeout = setTimeout(function() {
      fn.apply(cap, args);
    }, delay);
  }
}

module.exports = {
  findFirstNonRepeatedChar: findFirstNonRepeatedChar,
  reverseString: reverseString,
  reverseWords: reverseWords,
  toInteger: toInteger,
  itos: itos,
  permute: permute,
  combine: combine,
  debounce: debounce
};

assert.ok(findFirstNonRepeatedChar('teeter') === 'r');
assert.ok(findFirstNonRepeatedChar('tttttt') === undefined);
assert.ok(findFirstNonRepeatedChar('teeth') === 'h');
assert.ok(reverseWords('Do or do not, there is no try.') === 'try. no is there not, do or Do');
