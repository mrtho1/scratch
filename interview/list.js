'use strict';

var assert = require('assert');

function createList() {
  var _head;

  return {
    length: 0,
    add: function(data) {
      var node = {
        data: data,
        next: null
      },
      current = _head;

      if (!_head) {
        _head = node;
      } else {

        while (current.next) {
          current = current.next;
        }

        current.next = node;
      }

      ++this.length;
      
      return this;
    },
    get: function(index) {
      var count = 0, current = _head;

      while (current && count < index) {
        current = current.next;
        ++count;
      }

      return current ? current.data : undefined;
    }
  };
}

module.exports = createList;

var list = createList();
list.add(1).add(2).add(3);
assert.ok(list.get(0) === 1);
assert.ok(list.get(1) === 2);
assert.ok(list.get(2) === 3);
assert.ok(list.get(1000) === undefined);
