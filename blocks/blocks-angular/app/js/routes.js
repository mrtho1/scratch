'use strict';

routes.$inject = ['$routeProvider', '$locationProvider'];
function routes($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);

  $routeProvider.when('/', {
    templateUrl: 'views/main.html'
  }).otherwise({
    redirectTo: '/'
  });
}

module.exports = routes;
