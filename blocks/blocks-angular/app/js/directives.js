'use strict';

var angular = require('angular');

function mtBlockContainer() {
  return {
    restrict: 'A',
    templateUrl: 'views/blockContainer.html',
    controllerAs: 'vm',
    controller: function(blockService) {
      var WHITE = Number.parseInt("FFFFFF", 16);

      function padLeft(source, to, padChar) {
        var val = "" + source;
        to = to || 6;
        padChar = padChar || 0;

        while (val.length != to) {
          val = padChar + val;
        }

        return val;
      }

      function randomColor() {
        return "#" + padLeft(Math.round(Math.random() * WHITE).toString(16));
      }

      this.addBlock = function() {
        var width = Math.round(Math.random() * this.maxWidth);
        var height = Math.round(Math.random() * this.maxHeight);
        var top = Math.round(Math.random() * (this.maxHeight - height));
        var left = Math.round(Math.random() * (this.maxWidth - width));

        blockService.addBlock(top, left, width, height, randomColor());
      };

      this.activateBlock = function(id) {
        blockService.activate(id);
      };

      this.activateAll = function() {
        blockService.activateAll();
      };

      this.deactivateAll = function() {
        blockService.deactivateAll();
      };

      this.clear = function() {
        blockService.clear();
      };

      this.all = function() {
        return blockService.getAll();
      };

      this.init = function(width, height) {
        this.maxWidth = width;
        this.maxHeight = height;
      };
    },
    link: function(scope, element, attrs, ctrl) {
      var content = element[0].querySelector('.content');
      ctrl.init(content.clientWidth, content.clientHeight);
    }
  };
}

function mtBlock() {

  return {
    restrict: 'A',
    templateUrl: 'views/block.html',
    scope: {
      binding: '=mtBlock'
    },
    controllerAs: 'vm',
    controller: function(blockService) {
      var self = this;

      self.toggleActive = function() {
        blockService.toggleActive(self.block.id);
      };

      self.init = function(block) {
        self.block = block;
      }
    },
    link: function(scope, element, attrs, ctrl) {
      ctrl.init(scope.binding);
    }
  };
}

angular.module('blocks.directives', ['blocks.services'])
  .directive('mtBlock', mtBlock)
  .directive('mtBlockContainer', mtBlockContainer)
