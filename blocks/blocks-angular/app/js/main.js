'use strict';

var angular = require('angular');
require('angular-route');

require('./directives.js');
require('./services.js');

angular.element(document).ready(function() {
  var app = angular.module('blocks',
    ['ngRoute', 'blocks.directives', 'blocks.services']);
  app.config(require('./routes'));

  angular.bootstrap(document, ['blocks']);
});
