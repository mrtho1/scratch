'use strict';

var angular = require('angular');

angular.module('blocks.services', []).service('blockService', function() {
  var blocks = [];

  this.getAll = function() {
    return blocks;
  };

  this.getBlockById = function(id) {
    return blocks[id];
  };

  this.addBlock = function(top, left, width, height, color) {

    var block = {
      id: blocks.length,
      width: width,
      height: height,
      top: top,
      left: left,
      color: color,
      active: false
    };

    blocks.push(block);
    return block;
  };

  this.activate = function(id, active) {
    var block = blocks[id];

    if (block) {
      block.active = angular.isUndefined(active) ? true : active;
    }
  };

  this.deactivate = function(id) {
    this.activate(id, false);
  };

  this.toggleActive = function(id) {
    var block = blocks[id];

    if (block) {
      block.active = !block.active;
    }
  }

  this.activateAll = function() {
    blocks.forEach(function(block) {
      block.active = true;
    });
  };

  this.deactivateAll = function() {
    blocks.forEach(function(block) {
      block.active = false;
    });
  };

  this.clear = function() {
    blocks = [];
  }
});
