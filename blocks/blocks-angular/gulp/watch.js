var gulp        = require('gulp');
var refresh     = require('gulp-livereload');

// Watch
module.exports = function () {
  gulp.watch('app/views/**/*.html', ['copy:html']);
  gulp.watch('app/js/**/*', ['scripts']);
  gulp.watch('app/css/**/*.css', ['styles']);
};
