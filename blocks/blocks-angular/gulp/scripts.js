var gulp = require('gulp');
var browserify = require('browserify');
var refresh = require('gulp-livereload');
var rename = require('gulp-rename');
var transform = require('vinyl-transform');
var multipipe = require('multipipe');

function bundler(file) {
  var b = browserify(file, {
    extensions: ['.js'],
    debug: true,
    insertGlobalVars: true
  });

  b.require('./app/js/main.js', { expose: 'main' });

  return b.bundle();
}

module.exports = function() {

  var scripts = [
    gulp.src('app/js/main.js'),
    transform(bundler),
    gulp.dest('target/dist/js')
  ];

  if( global.lrserver ) {
    scripts.push(refresh(global.lrserver));
  }

  var scriptsFunction = multipipe.apply(this, scripts);

  function errorHandler(e) {
    console.log('ERROR', e);
  }

  scriptsFunction.on('error', errorHandler);

  return scriptsFunction;
};
