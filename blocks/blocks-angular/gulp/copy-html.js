var gulp 	   = require('gulp');
var refresh    = require('gulp-livereload');
var preprocess = require('gulp-preprocess');

module.exports = function () {
	return gulp.src('app/**/*.html')
		.pipe(preprocess({context: { dev: true }}))
		.pipe(gulp.dest('target/dist'))
		.pipe(refresh(global.lrserver));
};
