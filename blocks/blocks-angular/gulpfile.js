var gulp = require('gulp');

/**
 * Development
 */
var server = require('./gulp/server');
var scripts = require('./gulp/scripts');
var watch = require('./gulp/watch');
var copyHTML = require('./gulp/copy-html');
var styles = require('./gulp/styles');

gulp.task('server', server);
gulp.task('scripts', scripts);
gulp.task('watch', watch);
gulp.task('copy:html', copyHTML);
gulp.task('styles', styles);

gulp.task('build', [
  'scripts'
  ]);

gulp.task('test', [
  'build',
  'mock',
  'karma'
  ]);

gulp.task('default', [
  'server',
  'build',
  'styles',
  'copy:html',
  'watch'
  ]);
