(function() {

  var WHITE = Number.parseInt("FFFFFF", 16);
  var _blockCount = 0;

  function padLeft(source, to, padChar) {
    var val = "" + source;
    to = to || 6;
    padChar = padChar || 0;

    while (val.length != to) {
      val = padChar + val;
    }

    return val;
  }

  function randomColor() {
    return "#" + padLeft(Math.round(Math.random() * WHITE).toString(16));
  }

  function deactivate(block) {
    block.classList.remove("selected");
    block.innerHTML = "";
    block.removeAttribute('data-block-activated')
  }

  function activate(block, reset) {
    if (reset) {
      [].forEach.call(document.querySelectorAll(".block"), deactivate);
    }

    var blockId = Number.parseInt(block.dataset.blockId, 10);
    block.classList.add("selected");
    block.innerHTML = "<div class='block-text'>" + blockId + "</div>";
    block.setAttribute('data-block-activated', '');
  }

  function onActivateAllClick() {
    [].forEach.call(document.querySelectorAll('.block'), function(block) {
      activate(block, false);
    });
  }

  function onDeactivateAllClick() {
    [].forEach.call(document.querySelectorAll('.block'), function(block) {
      deactivate(block);
    });
  }

  function onBlockClick(e) {
    activate(e.target, true);
  }

  function onAddBlockClick() {
    var block = document.createElement("div");
    block.className = "block";

    var content = document.querySelector(".content");

    var width = Math.round(Math.random() * content.clientWidth);
    var height = Math.round(Math.random() * content.clientHeight);
    var top = Math.round(Math.random() * (content.clientHeight - height));
    var left = Math.round(Math.random() * (content.clientWidth - width));

    var color = randomColor();
    block.setAttribute('data-block-id', _blockCount);
    ++_blockCount;

    block.style.backgroundColor = color;
    block.style.width = width + "px";
    block.style.height = height + "px";
    block.style.top = top + "px";
    block.style.left = left + "px";

    block.addEventListener("click", onBlockClick);
    content.appendChild(block);
  }

  function onClearClick() {
    var squares = document.querySelectorAll(".content > .block");
    [].forEach.call(squares, function(square) {
      square.parentNode.removeChild(square);
    });
  }

  document.addEventListener("DOMContentLoaded", function load() {
    document.removeEventListener("DOMContentLoaded", load);
    document.querySelector("#add-block").addEventListener("click", onAddBlockClick);
    document.querySelector("#activate-all").addEventListener("click", onActivateAllClick);
    document.querySelector("#deactivate-all").addEventListener("click", onDeactivateAllClick);
    document.querySelector("#clear").addEventListener("click", onClearClick);
  });
})();
