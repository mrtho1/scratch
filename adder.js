var assert = require('assert');

function add() {

  var result = 0;
  addSave.toString = function() {
      return "" + result;
  };

  //valueOf is used to convert to a primitive
  //We return result here, but you can also
  //return addSave itself.  This will cause
  //the conversion to fail and it will fall back
  //on toString instead.
  //Not sure what implications one impl has
  //over the other.
  addSave.valueOf = function() {
    return result;
  };

  return addSave.apply(null, arguments);

  function addSave() {
    result = [].reduce.call(arguments, (function(value, x) {
      return value + x;
    }), result);

    return addSave;
  }
}

module.exports = add;

assert.ok(add(2, 5) == 7);
assert.ok(add(2)(5) == 7);
assert.ok(add(2)(5)(1, 3)(4) == 15);
assert.ok(add(2)(5)(1, 3)(4)(1, 1, 2)(1)()(1, 1) == 22);
