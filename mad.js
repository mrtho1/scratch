var readline = require('readline');
var assert = require('assert');

function gatherInput() {
  return new Promise(function(resolve, reject) {
    var rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    var nums = [];

    rl.on('line', function(line) {

      var num = Number.parseFloat(line);

      if (!Number.isNaN(num)) {
        nums.push(num);
      } else {
        rl.close();

        if (nums.length > 0) {
          resolve(nums);
        } else {
          reject('No values.');
        }
      }
    });
  });
}

function average(nums) {
  var sum = nums.reduce(function(memo, num) {
    return memo + num;
  }, 0);

  return sum / nums.length;
}

function mad(nums) {
  var avg = average(nums);
  var deltas = nums.map(function(num) {
    return Math.abs(avg - num);
  });

  return average(deltas);
}

module.exports = {
  mad: mad,
  gatherInput: gatherInput
};

assert.ok(mad([1, 2, 3, 4]) === 1);
assert.ok(mad([2, 3, 4, 5]) === 1);
